# Documentation

### Required:
- Node.js
- NPM

### Build setup:
1. Navigate to the repository root folder in terminal
2. ```npm install``` to install node modules
3. ```npm run build``` for production build OR ```npm run start``` for local development

### Webpack:
- Build tasks have been set up to handle:
 - SCSS -> CSS compilation
 - TODO Automatically apply browser prefixes (PostCSS with autoprefixer)
 - Concatentation and minification of js.
 - TODO Optimisation of images.
 - TODO Compilation of an svg sprite from a set of individual icon files to gain the benefits of browser caching.
- Check webpack.config.js if unsure where the build files and output files are.
- The ```/dist``` directory contains all the compiled production code and assets needed. All paths in the html treat this as the root folder.
- The ```/_src``` folder contains all unprocessed unminified development assets (work on these ones)

### SCSS styleguide:
- Don't @extend classes unless they are 'silent' e.g. ```%foo {} .bar{@extend %foo}```. This prevents unintended output (extending all usage of that class) as well as providing a clear flag to the next developer that a class is likely to be extended elsewhere, and will effect those instances.
- All SCSS uses BEM syntax http://csswizardry.com/2013/01/mindbemding-getting-your-head-round-bem-syntax/
- As per BEM, do not use ids for styling. ids are rarely necessary in js either and writing code that doesn't require them will usually result in more reusable modules.

### JS styleguide:
- JS is written in a modular reusable fashion, broken into separate files
- Bind js only to classes/ids prefixed with js-. Don't use these for styling
- Follow BEM syntax for classes
- Add is- and has- classes with js to control temporary styling conditions, don't apply styles with js directly.
- Build tasks concatenate and minify all js into one file
