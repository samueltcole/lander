const path = require("path");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ESLintPlugin = require("eslint-webpack-plugin");
const { CleanWebpackPlugin } = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const assetsDir = "_assets"

module.exports = {
  mode: "production",
  entry: "./_src/_assets/js/index.js",
  output: {
    path: path.resolve(__dirname, "./dist"),
    filename: `${assetsDir}/js/scripts.min.js`
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              ["@babel/preset-env", { targets: "defaults" }]
            ]
          }
        }
      },
      {
        test: /\.(scss|css|sass)$/,
        use: [
          MiniCssExtractPlugin.loader,
          "css-loader",
          "sass-loader"
        ]
      },
      {
        test: /\.(png|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        generator: {
          filename: `${assetsDir}/media/[name][ext][query]`
        }
      },
      {
        test: /\.svg$/i,
        type: 'asset/source'
      }
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new ESLintPlugin(),
    new HtmlWebpackPlugin({
      template: './_src/index.html',
      inject: 'body',
      filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename:  `${assetsDir}/css/styles.min.css`
    }),
    new FaviconsWebpackPlugin({
      logo: `./_src/${assetsDir}/logo.png`,
      prefix: '_assets/favicons/'
    })
  ]
};