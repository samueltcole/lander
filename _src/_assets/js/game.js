(function () {
    /////////////////////////////////////////////
    // # Objects
    /////////////////////////////////////////////    
    const LanderGame = {
        lastTFrame: 0,
        input: {
            up: false,
            down: false,
            left: false,
            right: false,
            spinBrake: false
        },
        camTarget: {
            x: 0,
            y: 0
        },
        zoom: 0.5,
        landMax: {
            aoa: 20,
            sv: 200,
            drot: 1
        },
        landWarn: {
            aoa: 15,
            sv: 150,
            drot: 0.75
        },
        tutorial: {
            rotate: false,
            outOfControl: false,
            thrust: false,
            hasLanded: false
        }
    };

    function Physics(x, y, dx, dy, rot, drot) {
        let physics = {
            x: x || 0,
            y: y || 0,
            dx: dx || 0,
            dy: dy || 0,
            rot: rot || -Math.Pi/2,
            drot: drot || 0,
            update: function(dTime) {
                let dTimeSeconds = dTime/1000;
                // update all values by adding a fraction of the delta 
                // equal to the fraction of a second that has passed 
                // since last frame
                this.x += (this.dx*dTimeSeconds);
                this.y += (this.dy*dTimeSeconds);
                this.rot += (this.drot*dTimeSeconds);
                
                // loop around rot
                if(this.rot > 2*Math.PI) {
                    this.rot -= 2*Math.PI;
                } else if(this.rot < 0) {
                    this.rot += 2*Math.PI;
                }
            }
        };
        return Object.assign(
            physics
        );
    }

    function Collider(points) {
        const initialPointArray = points || [{x: 0, y: 0}];
        let collider = {
            pointArray: initialPointArray.slice(0),
            colliding: false,
            update: function(physics) {
                for(let i=0; i<initialPointArray.length; i++) {
                    // translate point
                    this.pointArray[i] = {
                        x: initialPointArray[i].x + physics.x,
                        y: initialPointArray[i].y + physics.y
                    };
                    // rotate point
                    this.pointArray[i] = rotatePoint(physics.x, physics.y, this.pointArray[i], physics.rot);
                }
            },
            draw: function(ctx) {
                ctx.save();
                if(this.colliding) {
                    ctx.fillStyle = "#FF0000";
                } else {
                    ctx.fillStyle = "#00FF00";
                }
                ctx.beginPath();
                ctx.moveTo(this.pointArray[0].x, this.pointArray[0].y);
                for(let i = 1; i<this.pointArray.length; i++) {
                    ctx.lineTo(this.pointArray[i].x, this.pointArray[i].y);
                }
                ctx.closePath();
                ctx.fill(); 
                ctx.restore();
            },
            checkCollision: function(otherPoints) {
                const approx = toRadians(1);
                let numCollisions = 0;
                for(let i=0; i<this.pointArray.length; i++) {
                    let wn = getWindingNumber(this.pointArray[i], otherPoints);
                    if(wn > (2*Math.PI - approx)) {
                        numCollisions++;
                    }
                }
                if(numCollisions>0) {
                    this.colliding = true;
                    return true;
                } else {
                    this.colliding = false;
                    return false;
                }
            }
        };
        return Object.assign(
            collider
        );
    }

    function Ship(x, y, dx, dy, rot, drot, landed) {
        const physics = Physics(x, y, dx, dy, rot, drot);
        const radius = 50;
        const mainThrust = 300;              // units/sec/sec
        const rotThrust = Math.PI;        // rad/sec/sec
        let thrustingForward = false;
        let thrustingLeft = false;
        let thrustingRight = false;
        let normal;
        let aoa;
        let sv;
        let signal;
        const angle =  0.4 * Math.PI/2;
        const curveBase = 0.25;
        const curveSides = 0.75;
        const thrustPlumeRadius = radius/5;
        const sideThrustPlumeRadius = radius/10;

        let colliderPoly = [
            {
                x:radius, 
                y:0
            }, 
            {
                x: Math.cos(Math.PI - angle) * radius,
                y: Math.sin(Math.PI - angle) * radius
            },
            {
                x: Math.cos(Math.PI + angle) * radius,
                y: Math.sin(Math.PI + angle) * radius
            } 
        ];

        const collider = Collider(colliderPoly);

        let ship = {
            landed: landed || false,
            crashed: false,
            crashedTime: 0,
            physics,
            collider,
            normal,
            signal,
            aoa,
            sv,
            hadSignal: true,
            applyThrust: function(dTime) {
                if(!LanderGame.tutorial.rotate && (LanderGame.input.right || LanderGame.input.left || LanderGame.input.up || LanderGame.input.spinBrake)) {
                    rotateTutorial();
                }
                if(!LanderGame.tutorial.outOfControl && LanderGame.tutorial.rotate && (LanderGame.input.right || LanderGame.input.left)) {
                    outOfControlTutorial();
                }
                if(!LanderGame.tutorial.thrust && LanderGame.tutorial.outOfControl && LanderGame.input.spinBrake) {
                    window.setTimeout(function() {
                        thrustTutorial();
                    }, 1000);
                }
                let dTimeSeconds = dTime/1000;
                // update all delta values depending on current input 
                // state by adding a fraction of the relevant dv/s equal
                // to the fraction of a second that has passed since
                // last frame
                if(LanderGame.input.right && this.signal>0 && !this.landed && this.physics.drot < 5 && LanderGame.tutorial.rotate) {
                    physics.drot += (rotThrust*dTimeSeconds);
                    thrustingRight = true;
                } else {
                    thrustingRight = false;
                }
                if(LanderGame.input.left && this.signal>0 && !this.landed && this.physics.drot > -5 && LanderGame.tutorial.rotate) {
                    physics.drot -= (rotThrust*dTimeSeconds);
                    thrustingLeft = true;
                } else {
                    thrustingLeft = false;
                }
                if(LanderGame.input.up && this.signal>0 && !this.crashed && LanderGame.tutorial.thrust) {
                    physics.dx += (mainThrust * Math.cos(physics.rot) * dTimeSeconds);
                    physics.dy += (mainThrust * Math.sin(physics.rot) * dTimeSeconds);
                    thrustingForward = true;
                } else {
                    thrustingForward = false;
                }
                if(LanderGame.input.spinBrake && this.signal>0 && !this.crashed && !this.landed && LanderGame.tutorial.outOfControl) {
                    if(physics.drot > 0.5) {
                       physics.drot -= (rotThrust*5*dTimeSeconds);
                       thrustingLeft = true; 
                    } else if (physics.drot < -0.5) {
                        physics.drot += (rotThrust*5*dTimeSeconds);
                        thrustingRight = true; 
                    } else {
                        physics.drot = 0;
                    }
                }
            },
            draw: function(ctx) {
                ctx.save();
                ctx.translate(physics.x, physics.y);
                ctx.rotate(physics.rot);
                // TODO this has logic that should be moved out of draw
                if(this.crashed) {
                    if(this.crashedTime<11) {
                        ctx.fillStyle = "#fff";
                        ctx.beginPath();
                        ctx.arc(0, 0, radius*0.5*this.crashedTime, 0, 2 * Math.PI);
                        ctx.fill();
                        this.crashedTime++;
                    } else if (this.crashedTime > 60) {
                        restart();
                    } else {
                        this.crashedTime++;
                    }
                } else {
                    // ship shape
                    ctx.lineWidth = 0;
                    ctx.fillStyle = "#fff";
                    ctx.beginPath();
                    ctx.moveTo(radius, 0);
                    ctx.quadraticCurveTo(
                        Math.cos(angle) * radius * curveSides,
                        Math.sin(angle) * radius * curveSides,
                        Math.cos(Math.PI - angle) * radius,
                        Math.sin(Math.PI - angle) * radius
                    );
                    ctx.quadraticCurveTo(
                        -radius * curveBase, 
                        0,
                        Math.cos(Math.PI + angle) * radius,
                        Math.sin(Math.PI + angle) * radius
                    );
                    ctx.quadraticCurveTo(
                        Math.cos(-angle) * radius * curveSides,
                        Math.sin(-angle) * radius * curveSides,
                        radius, 0
                    );
                    ctx.fill();
                    // main thrust
                    if(thrustingForward) {
                        ctx.fillStyle = "#d4184c";
                        ctx.beginPath();
                        ctx.moveTo(-radius, thrustPlumeRadius);
                        ctx.lineTo(-2.25*radius, 0);
                        ctx.lineTo(-radius, -thrustPlumeRadius);
                        ctx.arc(-radius, 0, thrustPlumeRadius, -Math.PI/2, Math.PI/2);
                        ctx.fill();
                    }
                    // l/r/back thrust
                    if(thrustingLeft) {
                        ctx.fillStyle = "#fff";
                        ctx.beginPath();
                        ctx.moveTo(radius, 3*sideThrustPlumeRadius);
                        ctx.lineTo(radius-sideThrustPlumeRadius, 7*sideThrustPlumeRadius);
                        ctx.lineTo(radius-2*sideThrustPlumeRadius, 3*sideThrustPlumeRadius);
                        ctx.arc(radius-sideThrustPlumeRadius, 3*sideThrustPlumeRadius, sideThrustPlumeRadius, -Math.PI, 0);
                        ctx.fill();
                    }
                    if(thrustingRight) {
                        ctx.fillStyle = "#fff";
                        ctx.beginPath();
                        ctx.moveTo(radius, -3*sideThrustPlumeRadius);
                        ctx.lineTo(radius-sideThrustPlumeRadius, -7*sideThrustPlumeRadius);
                        ctx.lineTo(radius-2*sideThrustPlumeRadius, -3*sideThrustPlumeRadius);
                        ctx.arc(radius-sideThrustPlumeRadius, -3*sideThrustPlumeRadius, sideThrustPlumeRadius, 0, -Math.PI);
                        ctx.fill();
                    }
                }
                ctx.restore();
            },
            updateShipData: function(targetPhysics, targetRadius) {
                this.normal = getHeading(
                    {
                        x: this.physics.x,
                        y: this.physics.y
                    },
                    {
                        x: targetPhysics.x,
                        y: targetPhysics.y
                    }
                );
                this.aoa = Math.abs(toDegrees(Math.PI - Math.abs(this.physics.rot - this.normal)));
                this.sv = Math.abs(this.physics.dx) + Math.abs(this.physics.dy);
                let altitude = getDistance(this.physics.x, this.physics.y, targetPhysics.x, targetPhysics.y) - targetRadius;
                if(altitude < 400) {
                    this.signal = 100;
                } else {
                    this.signal = 100 - ((altitude - 400)/20);
                }
                if(this.signal<0) {
                    this.signal = 0;
                }

                if(this.signal <= 0) {
                    this.hadSignal = false;
                    gameMessage(`signal lost, too far from home - gravity will pull you back, or press <span class="u-hide-md-up">refresh</span><span class="u-hide-md-down">r</span> to restart`);
                } else if(this.signal <= 50) {
                    if (this.hadSignal == false) {
                        this.hadSignal = true;
                        gameMessage("signal restored");
                        // TODO replace with delay/fade out
                        window.setTimeout(function() {
                            gameMessage("");
                        }, 2500);
                    }
                } 
            },
            checkSafeLanded: function() {
                // landing is safe if total velocity, rotational velocity and angle of attack are low enough
                if(this.sv >= LanderGame.landMax.sv) {
                    gameMessage("crashed - too fast");
                    return false;
                } else if(Math.abs(this.physics.drot) >= LanderGame.landMax.drot) {
                    gameMessage("crashed - unstable");
                    return false;
                } else if(this.aoa >= LanderGame.landMax.aoa) {
                    gameMessage("crashed - bad landing");
                    return false;
                } else {
                    return true;
                }
            },
            safeLand: function() {
                this.landed = true;
                this.physics.drot = 0;
                this.physics.dx = 0;
                this.physics.dy = 0;
                gameMessage("nice work, successful landing!");
                LanderGame.tutorial.hasLanded = true;
                //TODO replace with delay/fade out
                window.setTimeout(function(){
                    gameMessage("");
                }, 2500);
            },
            unsafeLand: function() {
                this.landed = false;
                this.crashed = true;
                this.physics.dx = 0;
                this.physics.dy = 0;
            }
        };
        return Object.assign(
            ship
        );
    }

    function Planet(mass, x, y, radius, segments) {
        const minSegments = 10;
        mass = mass || 100;
        radius = radius || 200;
        segments = segments || minSegments;
        if(segments < minSegments) {
            segments = minSegments;
        }
        const physics = Physics(x, y, 0, 0);
        // Random planet terrain generation
        let colliderPoly = [];
        // heavily randomise the heights of 5 points
        const minHeight = y-(0.7*radius);
        const maxHeight = y-(1*radius);
        for(let i=0; i<minSegments; i++) {
            colliderPoly[i] = {x: x, y: randomBetween(minHeight, maxHeight)};
        }
        // interpolate between existing points until enough segments are created
        let pointer = 0;
        while(colliderPoly.length < segments) {
            // DEBUG
            // logColliderHeights(colliderPoly);
            if((pointer+1)>colliderPoly.length) {
                pointer = 0;
            }
            let firstInterp =  colliderPoly[pointer].y;
            let secondInterp;
            if((pointer+1) == colliderPoly.length) {
                secondInterp = colliderPoly[0].y;
            } else {
                secondInterp = colliderPoly[pointer+1].y;
            }
            colliderPoly.splice(
                pointer+1, 
                0, 
                { x: x, y: randomBetween(firstInterp, secondInterp) }
            );
            pointer = pointer + 2;
        }
        // DEBUG
        // function logColliderHeights(poly) {
        //     console.log("POLY");
        //     for(let i=0; i<poly.length; i++) {
        //         console.log(i + ": " + poly[i].y);
        //     }
        // }

        // rotate all points around the center to form a polygon with consistently spaced sides
        const radPerSegments = 2*Math.PI/segments;
        for(let i=0; i<segments; i++) {
            colliderPoly[i] = rotatePoint(x, y, colliderPoly[i], (Math.PI/2)+(radPerSegments*i));
        }

        const collider = Collider(colliderPoly);

        let planet = {
            physics,
            collider,
            radius,
            applyGravity: function(targetPhysics, dTime) {
                // calculate total force
                let dTimeSeconds = dTime/1000;
                let distance = getDistance(physics.x, physics.y, targetPhysics.x, targetPhysics.y);
                // let scaledDistance = distance*0.05; // scale down the distance so that gravity doesn't fall off so quickly
                // let dv = mass*dTimeSeconds / (scaledDistance * scaledDistance); // inverse square law of gravity
                let dv = mass*dTimeSeconds/7000;
                // divide into x and y components
                targetPhysics.dx += dv * ((physics.x - targetPhysics.x) / distance);
                targetPhysics.dy += dv * ((physics.y - targetPhysics.y) / distance);
            },
            draw: function(ctx) {
                ctx.save();
                ctx.fillStyle = "#d4184c";
                ctx.beginPath();
                ctx.moveTo(colliderPoly[0].x, colliderPoly[0].y);
                for(let i = 1; i<colliderPoly.length; i++) {
                    ctx.lineTo(colliderPoly[i].x, colliderPoly[i].y);
                }
                ctx.closePath();
                ctx.fill();
                ctx.restore();
            }
        };
        return Object.assign(
            planet
        );
    }

    function StarField(stars, width, x, y) {
        stars = stars || 100;
        width = width || 1000;
        let starLocs = [];
        let minX = x - (width/2);
        let maxX = x + (width/2);
        let minY = y - (width/2);
        let maxY = y + (width/2);
        let minR = 2;
        let maxR = 7;
        for(let i = 0; i<stars; i++) {
            let newStar = {x: 0, y: 0};
            newStar.x = Math.floor(randomBetween(minX, maxX));
            newStar.y = Math.floor(randomBetween(minY, maxY));
            newStar.r = Math.floor(randomBetween(minR, maxR));
            starLocs[i] = newStar;
        }
        let starField = {
            draw: function(ctx) {
                ctx.save();
                for(let i=0; i<starLocs.length; i++) {
                    ctx.fillStyle = "#fff";
                    ctx.beginPath();
                    ctx.arc(starLocs[i].x, starLocs[i].y, starLocs[i].r, 0, 2 * Math.PI);
                    ctx.fill();
                }
                ctx.restore();
            }
        };
        return Object.assign(
            starField
        );
    }
    
    /////////////////////////////////////////////
    // # Setup
    /////////////////////////////////////////////

    const canvas = document.getElementById("js-bg-canvas");
    const ctx = canvas.getContext("2d");
    let player;
    let homeworld;
    let stars;
    function setup() {
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        player = Ship(0, 0, 350, 0, 1.5*Math.PI, toRadians(12));
        homeworld = Planet(500000, 0, 1800, 1000, 100);
        stars = StarField(1000, 30000, 0, 1800);
        LanderGame.camTarget.x = 0;
        LanderGame.camTarget.y = 0;
        LanderGame.zoom = 0.25;
        // make the canvas fit the screen initally
        ctx.canvas.width  = window.innerWidth;
        ctx.canvas.height = window.innerHeight;
        // translate so that camTarget is the center
        ctx.translate(
            LanderGame.camTarget.x + ctx.canvas.width/2, 
            LanderGame.camTarget.y + ctx.canvas.height/2
        );
        ctx.scale(LanderGame.zoom, LanderGame.zoom);
    }
    
    /////////////////////////////////////////////
    // # Main loop, Update, Render
    /////////////////////////////////////////////
    function main(tFrame) {
        LanderGame.mainLoop = window.requestAnimationFrame(main);
        update(LanderGame.lastTFrame, tFrame);
        render();
    }
    
    function update(lastTFrame, tFrame) {
        let dTime = tFrame - lastTFrame;
        if(dTime < 1) {
          dTime = 1;  
        }
        // DEBUG: FPS counter
        // let FPS = 1000/dTime;
        // console.log("FPS: " + FPS);
        player.applyThrust(dTime);
        if(!player.landed) {
            homeworld.applyGravity(player.physics, dTime);
        }
        player.physics.update(dTime);
        player.collider.update(player.physics);
        player.updateShipData(homeworld.physics, homeworld.radius);
        let collided = player.collider.checkCollision(homeworld.collider.pointArray);
        if(collided && !player.landed && !player.crashed) {
            let landedSafe = player.checkSafeLanded();
            if(landedSafe) {
                player.safeLand();
            } else {
                player.unsafeLand();
            }
        }
        if(!collided) {
            player.landed = false;
        }
        LanderGame.lastTFrame = tFrame;
    }

    function render() {
        // translate camera by difference between ship and camTarget, 
        // then update camtarget
        ctx.translate(
            LanderGame.camTarget.x - player.physics.x,
            LanderGame.camTarget.y - player.physics.y
        );
        LanderGame.camTarget.x = player.physics.x;
        LanderGame.camTarget.y = player.physics.y;
        // draw sky
        ctx.save();
        ctx.fillStyle = "#181849";
        ctx.lineWidth = 0;
        let currentCanvasUnitWidth = (1/LanderGame.zoom)*ctx.canvas.width;
        let currentCanvasUnitHeight = (1/LanderGame.zoom)*ctx.canvas.height;
        ctx.fillRect(
            LanderGame.camTarget.x - currentCanvasUnitWidth/2, 
            LanderGame.camTarget.y - currentCanvasUnitHeight/2, 
            currentCanvasUnitWidth, 
            currentCanvasUnitHeight
        );
        ctx.restore();
        // draw objects
        stars.draw(ctx);
        homeworld.draw(ctx);
        player.draw(ctx);
        // DEBUG: draw colliders
        // homeworld.collider.draw(ctx);
        // player.collider.draw(ctx);
    }

    /////////////////////////////////////////////
    // # Input
    /////////////////////////////////////////////
    
    // Keyboard input
    document.addEventListener("keydown", function(e) {
        switch(e.key) {
            case "ArrowUp":
                LanderGame.input.up = true;
                break;
            case "ArrowDown":
                LanderGame.input.down = true;
                break;
            case "ArrowLeft":
                LanderGame.input.left = true;
                break;
            case "ArrowRight":
                LanderGame.input.right = true;
                break;
            case " ":
                LanderGame.input.spinBrake = true;
                break;
            case "r":
                restart();
                break;
            case "R":
                restart();
                break;
        }
        // DEBUG: Input handler 
        // console.log(`INPUT:\nu:${LanderGame.input.up} d:${LanderGame.input.down} l:${LanderGame.input.left} r:${LanderGame.input.right}`);
    });
    document.addEventListener("keyup", function(e) {
        switch(e.key) {
            case "ArrowUp":
                LanderGame.input.up = false;
                break;
            case "ArrowDown":
                LanderGame.input.down = false;
                break;
            case "ArrowLeft":
                LanderGame.input.left = false;
                break;
            case "ArrowRight":
                LanderGame.input.right = false;
                break;
            case " ":
                LanderGame.input.spinBrake = false;
                break;
        }
        // DEBUG: Input handler
        // console.log(`INPUT:\nu:${LanderGame.input.up} d:${LanderGame.input.down} l:${LanderGame.input.left} r:${LanderGame.input.right}`);
    });

    // On-screen button input
    let arrowButtons = document.getElementsByClassName("js-game-button");
    for(let i=0; i<arrowButtons.length; i++) {
        arrowButtons[i].addEventListener("mousedown", e => arrowButtonsPressHandler(e));
        arrowButtons[i].addEventListener("touchstart", e => arrowButtonsPressHandler(e));
        arrowButtons[i].addEventListener("mouseup", e => arrowButtonsReleaseHandler(e));
        arrowButtons[i].addEventListener("touchend", e => arrowButtonsReleaseHandler(e));
        arrowButtons[i].addEventListener("touchleave", e => arrowButtonsReleaseHandler(e));
        arrowButtons[i].addEventListener("mouseleave", e => arrowButtonsReleaseHandler(e));
    }
    function arrowButtonsPressHandler(e) {
        let buttonPressed = e.target.dataset.control;
        switch(buttonPressed) {
            case "up":
                LanderGame.input.up = true;
                break;
            case "down":
                LanderGame.input.down = true;
                break;
            case "left":
                LanderGame.input.left = true;
                break;
            case "right":
                LanderGame.input.right = true;
                break;
            case "brake":
                LanderGame.input.spinBrake = true;
                break;
            case "restart":
                restart();
                break;
        }
        // DEBUG: Input handler 
        // console.log(`INPUT:\nu:${LanderGame.input.up} d:${LanderGame.input.down} l:${LanderGame.input.left} r:${LanderGame.input.right}`);
    }
    function arrowButtonsReleaseHandler(e) {
        let buttonReleased = e.target.dataset.control;
        switch(buttonReleased) {
            case "up":
                LanderGame.input.up = false;
                break;
            case "down":
                LanderGame.input.down = false;
                break;
            case "left":
                LanderGame.input.left = false;
                break;
            case "right":
                LanderGame.input.right = false;
                break;
            case "brake":
                LanderGame.input.spinBrake = false;
                break;
        }
        // DEBUG: Input handler 
        // console.log(`INPUT:\nu:${LanderGame.input.up} d:${LanderGame.input.down} l:${LanderGame.input.left} r:${LanderGame.input.right}`);
    }
   
    /////////////////////////////////////////////
    // # Helper functions
    /////////////////////////////////////////////
    function randomBetween(min, max) {
        return Math.random() * (max - min) + min;
    }

    function toRadians(deg) {
        return deg * Math.PI / 180;
    }

    function toDegrees(rad) {
        return rad * 180 / Math.PI;
    }

    function getDistance(x1, y1, x2, y2) {
        let a = x1 - x2;
        let b = y1 - y2;
        return Math.sqrt(a*a + b*b);
    }

    function rotatePoint(cx, cy, p, radians) {
        let rotatedX = Math.cos(radians) * (p.x - cx) - Math.sin(radians) * (p.y - cy) + cx;
        let rotatedY = Math.sin(radians) * (p.x - cx) + Math.cos(radians) * (p.y - cy) + cy;
        let newP = {
            x: rotatedX,
            y: rotatedY
        };
        return newP;
    }

    // Returns the accumulated radians from taking headings from 
    // a point to each of the points in a polygon in order, as if
    // winding a string from the point around the polygon
    function getWindingNumber(point, polygon) {
        // TODO make DRYer
        let wn = 0;
        let currentHeading = getHeading(point, polygon[0]);
        let newHeading, diff;
        for(let i=1; i<polygon.length; i++) {
            newHeading = getHeading(point, polygon[i]);
            diff = newHeading - currentHeading;
            // If >180deg <-180, measuring wrong way
            if(diff < -Math.PI) {
                diff = diff + (2*Math.PI);
            } else if (diff > Math.PI) {
                diff = diff - (2*Math.PI);
            }
            wn = wn + diff;
            currentHeading = newHeading;
        }
        // traverse from the last point back to the first
        newHeading = getHeading(point, polygon[0]);
        diff = newHeading - currentHeading;
        // If >180deg <-180, measuring wrong way
        if(diff < -Math.PI) {
            diff = diff + (2*Math.PI);
        } else if (diff > Math.PI) {
            diff = diff - (2*Math.PI);
        }
        wn = wn + diff;
        return wn;
    }
    
    function getHeading(point1, point2) {
        let p2relOrigin = {};
        p2relOrigin.x = point2.x - point1.x;
        p2relOrigin.y = point2.y - point1.y;
        let heading = Math.atan2(p2relOrigin.y, p2relOrigin.x);
        // always make positive relative to x axis
        if(heading < 0) {
            heading = (2*Math.PI) + heading;
        }
        return heading;
    }

    const messageElem = document.getElementById("js-game-message");
    function gameMessage(message) {
        if(message.length > 0) {
            messageElem.innerHTML = message;
            messageElem.classList.add("is-active");
        } else {
            messageElem.classList.remove("is-active");
        }
    }

    /////////////////////////////////////////////
    // # Start
    /////////////////////////////////////////////
    restart();

    function restart() {
        // console.log("restarting game");
        gameMessage("");
        if(LanderGame.tutorial.thrust && !LanderGame.tutorial.hasLanded) {
            thrustTutorial();
        }
        window.cancelAnimationFrame( LanderGame.mainLoop );
        LanderGame.lastTFrame = performance.now();
        setup();
        main(LanderGame.lastTFrame);
    }

    // on resize restart game
    window.addEventListener("resize", function() {
        restart();
    });
    // on window focus restart game
    document.addEventListener("visibilitychange", function(){
        if(this.visibilityState === "visible") {
            restart();
        }
    });

    /////////////////////////////////////////////
    // # Tutorial
    /////////////////////////////////////////////
    window.setTimeout(function() {
        rotateTutorial();
    }, 5000);
    function rotateTutorial() {
        if(!LanderGame.tutorial.rotate) {
            LanderGame.tutorial.rotate = true;
            gameMessage("use left/right arrow keys to rotate");
        }
    }
    function outOfControlTutorial() {
        LanderGame.tutorial.outOfControl = true;
        window.setTimeout(function() {
            gameMessage(`to stop spinning, apply thrust in the other direction or <span class="u-hide-md-up">press square</span><span class="u-hide-md-down">hold space</span>`);
        }, 4000);
        window.setTimeout(function() {
            thrustTutorial();
        }, 9000);
    }
    function thrustTutorial() {
        LanderGame.tutorial.thrust = true;
        gameMessage("press up arrow to thrust");
        window.setTimeout(function() {
            gameMessage("try to land by slowing your orbit then softly touching down");
        }, 4000);
    }

    /////////////////////////////////////////////
    // # Non-game UI
    /////////////////////////////////////////////
    let modalButtons = document.getElementsByClassName("js-modal-button");
    let modal = document.getElementById("js-modal");
    for (let i = 0; i < modalButtons.length; i++) {
        modalButtons[i].addEventListener("click", function() {
            if(modalButtons[i].classList.contains("is-active")) {
                closeModal();
            } else {
                openModal();
            }
        });
    }
    function openModal() {
        for (let i = 0; i < modalButtons.length; i++) {
            modalButtons[i].classList.add("is-active");
            modalButtons[i].innerHTML = "close about";
        }
        modal.classList.add("is-active");
    }
    function closeModal() {
        for (let i = 0; i < modalButtons.length; i++) {
            modalButtons[i].classList.remove("is-active");
            modalButtons[i].innerHTML = "about";
        }
        modal.classList.remove("is-active");
        document.getElementById("js-bg-canvas").focus();
    }

})();
